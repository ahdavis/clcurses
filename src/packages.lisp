;;;; packages.lisp
;;;; Package header for CLCurses
;;;; Created on 9/11/2019
;;;; Created by Andrew Davis
;;;;
;;;; Copyright (C) 2019  Andrew Davis
;;;;
;;;; This program is free software: you can redistribute it and/or modify   
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.
;;;;
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;; 
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.


;;; define the package
(defpackage :clcurses
  (:use :common-lisp :cffi))

;;; enter the package and link with ncurses
(in-package :clcurses)
(define-foreign-library libncurses
  (:darwin (:or "libncurses.5.9.dylib" "libncurses.dylib"))
  (:unix (:or "libncurses.so.5.9" "libncurses.so"))
  (t (:default "libncurses")))
(use-foreign-library libncurses)

;;; end of file
